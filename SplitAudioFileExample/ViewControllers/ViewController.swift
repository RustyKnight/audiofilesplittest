//
//  ViewController.swift
//  SplitAudioFileExample
//
//  Created by Shane Whitehead on 31/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var transportButton: UIButton!
	
	fileprivate var service: TransportService?

	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		NotificationCenter.default.addObserver(forName: TransportService.transportStopped, object: nil, queue: .main) { (notification) in
			self.service = nil
			self.transportButton.setTitle("Transport", for: [])
		}
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}

	@IBAction func transport(_ sender: Any) {
		if let service = service {
			service.stop()
			transportButton.setTitle("Transport", for: [])
			return
		}
		
		do {
			service = TransportService()
			try service?.start()
			
			transportButton.setTitle("Stop", for: [])
		} catch let error {
			print("!! \(error.localizedDescription)")
		}
	}
	
}

