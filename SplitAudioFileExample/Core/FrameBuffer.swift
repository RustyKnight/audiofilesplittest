//
//  File.swift
//  AudioTest
//
//  Created by Shane Whitehead on 13/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

// The frame buffer reads a aac encoded file, frame by frame.  It removes the "magic header" and returns the frame
// to the caller
class FrameBuffer {
	let originalBuffer: Data
	var remainingBuffer: Data
	
	var nextFrame: Data?
	
	init?() throws {
		guard let url = Bundle.main.url(forResource: "sample", withExtension: "aac") else { return nil }
		do { originalBuffer = try Data(contentsOf: url) }
		
		remainingBuffer = originalBuffer
	}
	
	// Determines if the buffer has another frame avaliable
	func hasNext() -> Bool {
		readNextFrame()
		return nextFrame != nil
	}
	
	// Returns the next buffer
	func next() -> Data {
		return nextFrame!
	}
	
	// Reads the next from the file
	fileprivate func readNextFrame() {
		var size: Int = 0
		nextFrame = nil
		guard !remainingBuffer.isEmpty else { return }
		repeat {
			if remainingBuffer.count < 7 { return }
			if remainingBuffer[0] == 0xff && (remainingBuffer[1] & 0xf0) == 0xf0 {
				size |= (Int(remainingBuffer[3]) & 0x03) << 11
				size |= (Int(remainingBuffer[4])) << 3
				size |= (Int(remainingBuffer[5]) & 0xe0) >> 5
				break
			}
			remainingBuffer = remainingBuffer.advanced(by: 1)
		} while true
		
		size = min(size, remainingBuffer.count)
		
		nextFrame = remainingBuffer[0..<size]
		remainingBuffer = remainingBuffer.advanced(by: size - 1)
	}
}
