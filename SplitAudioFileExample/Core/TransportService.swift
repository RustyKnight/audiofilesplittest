//
//  TransportService.swift
//  AudioTest
//
//  Created by Shane Whitehead on 19/8/19.
//  Copyright © 2019 Shane Whitehead. All rights reserved.
//

import Foundation

// Psudo data transporter
// This is where the audio frame would be packaged
// and sent to the client using TCP
class TransportService {
	
	static let transportStopped = Notification.Name("TransportService.stopped")
	
	fileprivate var stopped: Bool = true
	// The frame buffer makes it easier to grab each audio from
	// from the file and supply it to our code
	fileprivate var frameBuffer: FrameBuffer?
	
	func start() throws {
		guard stopped else { return }
		frameBuffer = try FrameBuffer()
		stopped = false
		DispatchQueue.global(qos: .userInitiated).async {
			self.transportData()
		}
	}
	
	func stop() {
		stopped = true
		frameBuffer = nil
		NotificationCenter.default.post(Notification(name: TransportService.transportStopped))
	}

	func transportData() {
		defer {
			stop()
		}
		guard let frameBuffer = frameBuffer else { return }

		while !stopped && frameBuffer.hasNext() {
			let data = frameBuffer.next()
			print("Frame size = \(data.count)")
			// Write frame header
			// Write frame data
			Thread.sleep(forTimeInterval: 0.1)
		}
	}

}
